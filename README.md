# Django webpack
Django Project

> A Vue.js project

## Build Vue

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

---

## Django

## Virtual env
    python -m venv .venv
    . .venv/bin/activate
    pip install -r requirements.txt

## Manage
migrate db

    ./manage.py migrate
load sample data

    ./manage.py loaddata sample
makemessages

    ./manage.p y makemessages -i assets/ -i .venv/ -sa
run development server

    ./manage.py runserver

### Localization
create

    ./manage.py makemessages -i assets/ -i .venv/ -s -l de    
compile

    ./manage.py compilemessages -l de

### Static files
build scss

    ./manage.py compilescss
collect static

    ./manage.py collectstatic -i *.scss -i *.map

---

###setup

#### CORS
[stackoverflow](https://stackoverflow.com/questions/35760943/how-can-i-enable-cors-on-django-rest-framework/35761458#35761458
 "Custom middleware")


##### © [a.Signz] 2018 #####
[a.Signz]: https://asignz.com
