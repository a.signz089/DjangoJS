"""DjangoJS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import RedirectView
from django.views.generic import TemplateView
from rest_framework import routers

from .views import index as index_view
from api import views as api
from Blog import views as Blog
from Snippets import views as Snippet


favicon_view = RedirectView.as_view(url='/static/favicon.ico', permanent=True)

router = routers.DefaultRouter()
router.register(r'users', api.UserViewSet)
router.register(r'posts', Blog.BlogViewSet)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/blog/$', Blog.blog_list),
    url(r'^api/blog/(?P<pk>[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})/$',
        Blog.post_detail),
    url(r'^snippets/$', Snippet.SnippetList.as_view()),
    url(r'^api/', include(router.urls)),
    url(r'^auth/', include('rest_framework.urls')),
    url(r'^$', index_view, name='index'),
    url(r'^snippets/', include('Snippets.urls')),
    url(r'^favicon\.ico$', favicon_view),
    url(r'^robots.txt$', TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
        name="robots_txt"),
]
if settings.DEBUG:
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
