from django.contrib.auth.models import User, Group
from rest_framework import serializers

from .models import Person


class ProfileInlineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ('id', 'bio')


class UserProfileSerializer(serializers.ModelSerializer):
    person = ProfileInlineSerializer()

    class Meta:
        model = User
        fields = ('url', 'username', 'person')

    def create(self, validated_data):
        person_data = validated_data.pop('person')
        user = User.objects.create(**validated_data)
        Person.object.create(owner=user, **person_data)
        return user

    def update(self, instance, validated_data):
        person_data = validated_data.pop('person')
        # Unless the application properly enforces that this field is
        # always set, the follow could raise a `DoesNotExist`
        person = instance.person

        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        instance.save()

        person.bio = person_data.get('bio', person.bio)
        person.birth_date = person_data.get('birth_date', person.birth_date)
        person.save()

        return instance
