from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import SnippetList, SnippetDetail, SnippetHighlight


urlpatterns = [
    url(r'^$', SnippetList.as_view(), name='snippet-list'),
    url(r'^(?P<pk>[0-9]+)/$', SnippetDetail.as_view(), name='snippet-detail'),
    url(r'^(?P<pk>[0-9]+)/html/$', SnippetHighlight.as_view(), name='snippet-highlight'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
