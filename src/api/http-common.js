import axios from 'axios'
import cachios from 'cachios'

// window.axios.defaults.headers.common = {'X-Requested-With': 'XMLHttpRequest'}
// window.axios.defaults.baseURL = (process.env.NODE_ENV !== 'production') ? 'http://localhost:8080/api' : ''

const HTTP = axios.create({
  baseURL: (process.env.NODE_ENV !== 'production') ? 'http://localhost:8080' : '',
  headers: {
    'X-Requested-With': 'XMLHttpRequest'
  }
})

export const API = cachios.create(HTTP, {
  stdTTL: 30
})
