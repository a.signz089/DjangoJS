import Vue from 'vue'
import Router from 'vue-router'
import error404 from '@/components/site/404'
import HelloWorld from '@/components/HelloWorld'
import YTtest from '@/components/yt'
import Blog from '@/components/Blog'
import Snippet from '@/components/Snippet'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/yt',
      name: 'YTtest',
      component: YTtest
    },
    {
      name: 'Blog',
      path: '/blog/:id?',
      component: Blog
    },
    {
      name: 'Snippet',
      path: '/snippet/:id?',
      component: Snippet
    },
    {
      path: '/:id',
      component: error404,
      name: '404'
    }
  ]
})
